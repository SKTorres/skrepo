package com.qa.client;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.Header;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

public class RestClient {
	
	
		// 1.GET method without Headers
		public CloseableHttpResponse get(String url) throws ClientProtocolException, IOException {
		
		//--> This will create a default HTTP client(a simple client will be created)
		CloseableHttpClient httpClient = HttpClients.createDefault(); 
		
		//-->This will create a get connection with the given url 
		HttpGet httpget= new HttpGet(url); //Http GET request 
		CloseableHttpResponse closeableHttpResponse= httpClient.execute(httpget); //Hit the GET URL
		
		//An Http response has 3 items. They are Http status, the JSON response and the Headers.
		return closeableHttpResponse;  
				
	}
	
		// 2.GET method with Headers
		public CloseableHttpResponse get(String url, HashMap<String, String> headerMap) throws ClientProtocolException, IOException {
		
		//--> This will create a default HTTP client(a simple client will be created)
		CloseableHttpClient httpClient = HttpClients.createDefault(); 
		
		//-->This will create a get connection with the given url 
		HttpGet httpget= new HttpGet(url); //Http GET request 
		
		for(Map.Entry<String,String> entry : headerMap.entrySet()) {
			httpget.addHeader(entry.getKey(), entry.getValue());
		}
		
		CloseableHttpResponse closeableHttpResponse= httpClient.execute(httpget); //Hit the GET URL
		
		//An Http response has 3 items. They are Http status, the JSON response and the Headers.
		return closeableHttpResponse;  
				
	}
		
		public CloseableHttpResponse post(String url, String entityString, HashMap<String, String> headerMap) throws ClientProtocolException, IOException {
			
			CloseableHttpClient httpClient= HttpClients.createDefault();
			HttpPost httpPost = new HttpPost(url);
			
			httpPost.setEntity(new StringEntity(entityString));
			
			for(Map.Entry<String, String> entry : headerMap.entrySet()) {
				httpPost.addHeader(entry.getKey(), entry.getValue());
			}
			
			CloseableHttpResponse closeableHttpResponse = httpClient.execute(httpPost);
			return closeableHttpResponse;
			
		}
		
		
	
}
