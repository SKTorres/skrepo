//Not working


package com.qa.tests;

import static org.testng.Assert.assertTrue;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

//import System.IO;
import java.io.File;
import java.io.FileInputStream;
import java.nio.file.Files;

import java.nio.file.Path;
import java.nio.file.Paths;

import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;

import org.sikuli.script.FindFailed;
import org.sikuli.script.Key;

public class HandleDynamicTable{
	public static void main(String[] args) throws Exception {
		
		System.setProperty("webdriver.chrome.driver", "C:\\VP\\tools\\chromedriver_win32\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();

		driver.manage().deleteAllCookies();
		driver.manage().window().maximize();
		driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.get("https://ui.freecrm.com/");
		
		WebDriverWait wait=new WebDriverWait(driver,30);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@placeholder='E-mail address']")));
		
		driver.findElement(By.xpath("//input[@placeholder='E-mail address']")).sendKeys("snehashishkumar92@gmail.com");
		driver.findElement(By.xpath("//input[@placeholder='Password']")).sendKeys("Test123");
		driver.findElement(By.xpath("//div[@class='ui fluid large blue submit button' and contains(text(), 'Login')]")).click();
		
		wait=new WebDriverWait(driver,30);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[@class='item-text' and contains(text(), 'Contacts')]")));
		
		driver.findElement(By.xpath("//span[@class='item-text' and contains(text(), 'Contacts')]")).click();
		
		//table[@class='ui celled sortable striped table custom-grid table-scroll']/tbody/tr[2]/td[2]
		Thread.sleep(3000);
		wait=new WebDriverWait(driver,30);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//table[@class='ui celled sortable striped table custom-grid table-scroll']/tbody/tr[1]/td[2]")));
		
		String beforeXpath="//table[@class='ui celled sortable striped table custom-grid table-scroll']/tbody/tr[";
		String afterXpath="]/td[2]";
		
		List<WebElement> list= driver.findElements(By.xpath("//table[@class='ui celled sortable striped table custom-grid table-scroll']/tbody/tr"));
		int n,i;
		n=list.size();
		System.out.println("Number of rows in table is - "+n);
		String name;
		for(i=1;i<=n;i++){
			name=driver.findElement(By.xpath(beforeXpath+i+afterXpath)).getText();
			if(name.contains("Rocky Lal"))
				driver.findElement(By.xpath(beforeXpath+i+afterXpath)).click();
		}
//		elementToBeClickable
//		Thread.sleep(3000);
//		wait =new WebDriverWait(driver,20);
//		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//td[contains(text(), 'Rocky Lal')]//preceding-sibling::td/div/input[@name='id']")));
//		driver.findElement(By.xpath("//td[contains(text(), 'Rocky Lal')]//preceding-sibling::td/div/input[@name='id']")).click();

		Thread.sleep(3000);
		
		driver.quit();
	}
}
