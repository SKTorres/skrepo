//Actions class functionalities such as right click(contextClick()), 
//clicking an option from multiple drop-down options, etc.  

package com.qa.tests;

import static org.testng.Assert.assertTrue;

import java.awt.AWTException;
import java.awt.Frame;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

//import System.IO;
import java.io.File;
import java.nio.file.Files;

import java.nio.file.Path;
import java.nio.file.Paths;

import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;


import org.sikuli.script.FindFailed;
import org.sikuli.script.Key;

public class ActionClass{
	public static void main(String[] args) throws Exception {
		
		System.setProperty("webdriver.chrome.driver", "C:\\VP\\tools\\chromedriver_win32\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		
		driver.manage().window().maximize();
		driver.manage().deleteAllCookies();
		driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.get("http://swisnl.github.io/jQuery-contextMenu");
		
		driver.findElement(By.xpath("//a[contains(text(), 'Demo gallery')]")).click();
		
		WebDriverWait wait=new WebDriverWait(driver,30);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[contains(text(), 'right click me')]")));
		
		Actions act=new Actions(driver);
		act.contextClick(driver.findElement(By.xpath("//span[contains(text(), 'right click me')]")))
		.click(driver.findElement(By.xpath("//li[@class='context-menu-item context-menu-icon context-menu-icon-copy']")))
		.build().perform();
		
		Alert alt=driver.switchTo().alert();
		Thread.sleep(1000);
		System.out.println(alt.getText());
		alt.accept();
		act.contextClick(driver.findElement(By.xpath("//span[contains(text(), 'right click me')]"))) 
		.build().perform();
		WebElement ele=driver.findElement(By.cssSelector(".context-menu-icon-paste"));
		String str=ele.getText();
		System.out.println(str);
		str="";
		List<WebElement> list=driver.findElements(By.xpath("//ul[@class='context-menu-list context-menu-root']/li[contains(@class, 'context-menu-item context-menu-icon')]/span"));
		System.out.println("No of options are - "+list.size());
		System.out.println("The options are :-");
		for(int i=0;i<list.size();i++) {
			str=list.get(i).getText();
			System.out.println(str);
		}
		
		driver.quit();
		
	}
}