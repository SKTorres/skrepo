//Handling the drop down using the Select class

package com.qa.tests;

import static org.testng.Assert.assertTrue;

import java.awt.AWTException;
import java.awt.Frame;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

//import System.IO;
import java.io.File;
import java.nio.file.Files;

import java.nio.file.Path;
import java.nio.file.Paths;

import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;

import net.bytebuddy.agent.builder.AgentBuilder.InitializationStrategy.SelfInjection.Split;

import org.sikuli.script.FindFailed;
import org.sikuli.script.Key;

public class DropDownHandle{
	public static void main(String[] args) throws Exception {
		
		System.setProperty("webdriver.chrome.driver", "C:\\VP\\tools\\chromedriver_win32\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		
		driver.manage().window().maximize();
		driver.manage().deleteAllCookies();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
		driver.get("https://www.facebook.com");
		
		String dob="06-Nov-1992";
		String dobArr[]=dob.split("-");
		int date=Integer.parseInt(String.valueOf(dobArr[0]));
//		System.out.println(date);
		dobArr[0]=Integer.toString(date);
			
		WebElement day=driver.findElement(By.id("day"));
		WebElement month=driver.findElement(By.id("month"));
		WebElement year=driver.findElement(By.id("year"));
		
		selectFromDropdown(day,dobArr[0]);
		selectFromDropdown(month,dobArr[1]);
		selectFromDropdown(year,dobArr[2]);		
		
		Select se= new Select(year);
		//To check if the drop-down is a multiple select option or not
		System.out.println(se.isMultiple());
		
		List<WebElement> list=se.getOptions();
		System.out.println(list.size()-1);
		for(int i=0;i<list.size();i++) {
			System.out.println(list.get(i).getText());
			if(list.get(i).getText().equals("1994")) {
				list.get(i).click();
				break;
			}
		}
		
		Thread.sleep(1000);
		driver.quit();
		
	}
	public static void selectFromDropdown(WebElement ele, String val) {
		Select se= new Select(ele);
		se.selectByVisibleText(val);
		
	}
}