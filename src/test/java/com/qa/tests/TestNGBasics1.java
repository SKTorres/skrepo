package com.qa.tests;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

//	The hierarchy of execution
//	1. BeforeSuite
//	2. BeforeTest
//	3. BeforeClass
//	4. BeforeMethod
//	5. Test
//	6. AfterMethod
//	7. AfterClass
//	8. AfterTest
//	9. AfterSuite
	

public class TestNGBasics1 {
	
	WebDriver driver;
	
	@BeforeSuite
	public void setUp() {
		System.out.println("@BeforeSuite--> Set up system property for chrome.");
	}
	
	@BeforeTest
	public void launchBrowser() {
		System.out.println("@BeforeTest--> Launching browser.");
	}
	
	@BeforeClass
	public void login() {
		System.out.println("@BeforeClass--> Logging in.");
	}
	
	@BeforeMethod
	public void enterUrl() {
		System.out.println("@BeforeMethod.");
		System.setProperty("webdriver.chrome.driver", "C:\\VP\\tools\\chromedriver_win32\\chromedriver.exe");
		driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().deleteAllCookies();
		driver.manage().timeouts().implicitlyWait(25, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
		driver.get("http://www.google.com");
	}
	
	@Test(priority=0, groups="Title")
	public void googleTitleTest() {
		System.out.println("@Test1--> Google title test.");
	}
	
	@Test(priority=2, groups="Page")
	public void googlePageTest() {
		System.out.println("@Test2--> Google page test.");
	}
	
	@Test(priority=1, groups="Search")
	public void googleSearchTest() {
		System.out.println("@Test3--> Google search test.");
	}
	
	@Test(priority=3, groups="Simple")
	public void simpleTest1() {
		System.out.println("@Test4--> Simple test1.");
	}
	
	@Test(priority=3, groups="Simple")
	public void simpleTest2() {
		System.out.println("@Test5--> Simple test2.");
	}
	
	@Test(priority=3, groups="Simple")
	public void simpleTest3() {
		System.out.println("@Test6--> Simple test3.");
	}
	
	@AfterMethod
	public void logOut() {
		System.out.println("@AfterMethod--> Logging out.");
		driver.quit();
	}
	
	@AfterClass
	public void deleteAllCookies() {
		System.out.println("@AfterClass--> Deleting cookies.");
	}
	
	@AfterTest
	public void closeBrowser() {
		System.out.println("@AfterTest--> Closing browser.");
	}
	
//	@AfterSuite
//	public void quitDriver() {
//		System.out.println("Driver quit.");
//	}
	
}
