//Not working


package com.qa.tests;

import static org.testng.Assert.assertTrue;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

//import System.IO;
import java.io.File;
import java.io.FileInputStream;
import java.nio.file.Files;

import java.nio.file.Path;
import java.nio.file.Paths;

import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;

import org.sikuli.script.FindFailed;
import org.sikuli.script.Key;

public class GoogleSearchTest{
	public static void main(String[] args) throws Exception {
		
		System.setProperty("webdriver.chrome.driver", "C:\\VP\\tools\\chromedriver_win32\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		
		driver.manage().deleteAllCookies();
		driver.manage().window().maximize();
		driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.get("https://www.google.com/");
		
		driver.findElement(By.xpath("//input[@class='gLFyf gsfi']")).sendKeys("testing");
		List<WebElement> list=driver.findElements(By.xpath("//ul[@role='listbox']/li/descendant::b"));
		System.out.println("No. of suggestions are "+list.size());
		
		int i;
		String str;
		WebDriverWait wait=new WebDriverWait(driver,30);
		for(i=0;i<list.size();i++) {
//			Thread.sleep(1000);
			try {
				driver.findElement(By.xpath("//input[@class='gLFyf gsfi']")).click();
//			wait.until(ExpectedConditions.refreshed(ExpectedConditions.stalenessOf(list.get(i))));
				str=list.get(i).getText();
				System.out.println(str);
				if(str.contains("interview questions")){
					System.out.println("Found at "+(i+1)+"th position.");
					clickOn(driver,list.get(i),20);
					break;
				}
			}
			catch(org.openqa.selenium.StaleElementReferenceException e) {
				System.out.println("Catch");
			}
		}
			
		Thread.sleep(3000);	
//		System.out.println(str);
		driver.quit();
		
	}
	public static void clickOn(WebDriver driver, WebElement locator, int timeout){
		
		new WebDriverWait(driver,timeout).ignoring(StaleElementReferenceException.class)
		.until(ExpectedConditions.elementToBeClickable(locator));
		locator.click();
	}
}