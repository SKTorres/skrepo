package com.qa.tests;

import static org.testng.Assert.assertEquals;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.qa.base.TestBase;
import com.qa.client.RestClient;
import com.qa.data.Users;

public class PostAPITest extends TestBase{
	
	TestBase testBase;
	String serviceUrl;
	String apiUrl;
	String url;
	RestClient restClient;
	CloseableHttpResponse closeableHttpResponse;
	
	
	@BeforeMethod
	public void setUP() {
		testBase = new TestBase();
		serviceUrl = prop.getProperty("URL");
		apiUrl = prop.getProperty("serviceURL");
		
		url = serviceUrl + apiUrl;
		//https://reqres.in/api/users
		
	}
	
	@Test
	public void postAPITest() throws JsonGenerationException, JsonMappingException, IOException {
		restClient = new RestClient();
		
		HashMap<String, String> headerMap = new HashMap<String, String>();
		headerMap.put("Content-Type", "application/json");
		
		//Jackson API - Used for Marshalling and Unmarshalling(Conversion of Java Object to a JSON file and vice-versa) 
		ObjectMapper mapper = new ObjectMapper();
		Users users= new Users("SK", "Topaz");
		
		//Java Object to JSON file
		mapper.writeValue(new File("C:\\Users\\KumarSne\\eclipse-workspace\\restapi\\src\\main\\java\\com\\qa\\data\\users.json"), users);
		
		//Java Object to JSON in String (Marshaling)
		String usersJsonString = mapper.writeValueAsString(users);
		System.out.println("Marshaling done Java object to JSON String --> "+usersJsonString);
		
		closeableHttpResponse = restClient.post(url, usersJsonString, headerMap);
		
		//1.Status code: 
		int statusCode = closeableHttpResponse.getStatusLine().getStatusCode();
		System.out.println("statusCode = "+statusCode );
		assertEquals(statusCode, RESPONSE_STATUS_CODE_201);
		
		//2. JSON String:
		String responseString = EntityUtils.toString(closeableHttpResponse.getEntity(), "UTF-8");
		JSONObject responseJson= new JSONObject(responseString);
		
		System.out.println("The response from API is "+responseJson);
		
		//JSON to Java (Unmarshalling)
		Users usersResObj = mapper.readValue(responseString, Users.class);
		System.out.println(usersResObj);
		
		assertEquals(usersResObj.getName(),users.getName());
		assertEquals(usersResObj.getJob(),users.getJob());
		
		System.out.println("Response ID is "+usersResObj.getId());
		System.out.println("Response createdAt is "+usersResObj.getCreatedAt());
		
		
	}
	
	
}
