//Some pages have an authentication pop-up as soon as we hit the URL of that page.
//To login to that page, the following code can be used.

package com.qa.tests;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import java.awt.AWTException;
import java.awt.Frame;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

//import System.IO;
import java.io.File;
import java.nio.file.Files;

import java.nio.file.Path;
import java.nio.file.Paths;

import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;


import org.sikuli.script.FindFailed;
import org.sikuli.script.Key;

public class BasicAuth{
	public static void main(String[] args) throws Exception {
		
		System.setProperty("webdriver.chrome.driver", "C:\\VP\\tools\\chromedriver_win32\\chromedriver.exe");
		WebDriver driver=new ChromeDriver();
		
		driver.manage().window().maximize();
		driver.manage().deleteAllCookies();
		driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		
		//The following is the way to provide the login credentials at the time of hitting the url itself. 
		//The http:// is followed by the username, followed by a colon, followed by the password, 
		//followed by @, followed by the rest of the url.
		//http://[username]:[password]@[rest of the url].com
		
		driver.get("http://admin:admin@the-internet.herokuapp.com/basic_auth");
		
		assertTrue(driver.findElement(By.xpath("//p[contains(text(), 'Congratulations!')]")).isDisplayed());
		Thread.sleep(2000);
		driver.quit();
		System.out.println("Driver quit.");
	}
}
