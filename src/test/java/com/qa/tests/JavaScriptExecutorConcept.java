package com.qa.tests;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import java.awt.AWTException;
import java.awt.Frame;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

//import System.IO;
import java.io.File;
import java.nio.file.Files;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;


import org.sikuli.script.FindFailed;
import org.sikuli.script.Key;

public class JavaScriptExecutorConcept{
	public static void main(String[] args) throws Exception {
		
		System.setProperty("webdriver.chrome.driver", "C:\\VP\\tools\\chromedriver_win32\\chromedriver.exe");
		WebDriver driver=new ChromeDriver();
		
		driver.manage().window().maximize();
		driver.manage().deleteAllCookies();
		driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.get("https://ui.freecrm.com");
		
		WebDriverWait wait=new WebDriverWait(driver,30);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@placeholder='E-mail address']")));
		driver.findElement(By.xpath("//input[@placeholder='E-mail address']")).sendKeys("snehashishkumar92@gmail.com");
		driver.findElement(By.xpath("//input[@name='password']")).sendKeys("Test123"); //Wrong password given. Correct password - Test123
//		driver.findElement(By.xpath("//div[contains(text(), 'Login') and @class='ui fluid large blue submit button']")).click();
		 
		WebElement ele=driver.findElement(By.xpath("//div[contains(text(), 'Login') and @class='ui fluid large blue submit button']"));
		
//		flashElement(driver, ele);
		
//		drawBorder(ele,driver);
//		File file=((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
//		FileUtils.copyFile(file,new File("C:\\Users\\KumarSne\\Desktop\\Auto\\LoginBorder.png"));
//		file=new File("C:\\Users\\KumarSne\\Desktop\\Auto\\LoginBorder.png");
//		System.out.println(file.exists());
//		assertTrue(file.exists());
//		Generate a customized alert using JavascriptExecutor
//		generateAndHandleAlert(driver,"There is an issue with the login button of the page.");

//		Click on an web element using JavascriptExecutor
		clickElementUsingJS(driver, ele);
		
//		Refresh the page using JavascriptExecutor
		refreshBrowserByJS(driver);

//		Get the page title using JavascriptExecutor
		System.out.println("Page title is - "+getPageTitleJS(driver));
		
//		Get the page text using JavascriptExecutor
		System.out.println("Page content is - "+getPageInnerText(driver));
		
//		To scroll to the bottom of the page using JavascriptExecutor
		scrollPageDown(driver);
//		Thread.sleep(2000);
		
		ele=driver.findElement(By.xpath("//div[contains(text(), 'Exchange Rates')]"));
		Thread.sleep(2000);
//		Scroll till an element on the webpage comes into view
		scrollIntoView(driver, ele);
		Thread.sleep(2000);
		driver.quit();
		
	}
	
	public static void flashElement(WebDriver driver, WebElement ele) throws InterruptedException{
		String bgcolor=ele.getCssValue("backgroundColor");
//		System.out.println(bgcolor);
		for(int i=0;i<5;i++) {
//			Changing the colour of the element using JavascriptExecutor
			ChangeColor(ele,driver,"rgba(200, 0, 0, 1)");
			Thread.sleep(500);
			ChangeColor(ele,driver,bgcolor);
			Thread.sleep(500);
		}		
	}
	
	public static void ChangeColor(WebElement ele,WebDriver driver,String color) {
		JavascriptExecutor js=((JavascriptExecutor) driver);
		js.executeScript("arguments[0].style.backgroundColor = '"+color+"'", ele);
	}
	
	public static void drawBorder(WebElement ele, WebDriver driver) throws InterruptedException {
		JavascriptExecutor js=((JavascriptExecutor) driver);
		js.executeScript("arguments[0].style.border = '3px solid red'", ele);
		Thread.sleep(2000);
	}
	
	public static void generateAndHandleAlert(WebDriver driver, String msg) throws InterruptedException {
		JavascriptExecutor js=((JavascriptExecutor) driver);
		js.executeScript("alert('"+msg+"')");
		Thread.sleep(2000);
		Alert alt=driver.switchTo().alert();
		alt.accept();
		Thread.sleep(2000);
	}
	
	public static void clickElementUsingJS(WebDriver driver, WebElement ele) throws InterruptedException {
		JavascriptExecutor js=((JavascriptExecutor) driver);
		js.executeScript("arguments[0].click();", ele);
		Thread.sleep(2000);
	}
	
	public static void refreshBrowserByJS(WebDriver driver) {
		JavascriptExecutor js=((JavascriptExecutor) driver);
		js.executeScript("history.go(0)");
	}
	
	public static String getPageTitleJS(WebDriver driver) {
		JavascriptExecutor js=((JavascriptExecutor) driver);
		String title=js.executeScript("return document.title;").toString();
		return title;
	}
	
	public static String getPageInnerText(WebDriver driver) {
		JavascriptExecutor js=((JavascriptExecutor) driver);
		String str=js.executeScript("return document.documentElement.innerText;").toString();		
		return str;
	}
	
	public static void scrollPageDown(WebDriver driver) {
		JavascriptExecutor js= ((JavascriptExecutor) driver);
		js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
	}
	
	public static void scrollIntoView(WebDriver driver, WebElement ele) {
		JavascriptExecutor js=((JavascriptExecutor) driver);
		js.executeScript("arguments[0].scrollIntoView(true)", ele);
	}
}