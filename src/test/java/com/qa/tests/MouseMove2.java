//Very erratic. Works intermittently.


package com.qa.tests;

import static org.testng.Assert.assertTrue;

import java.awt.AWTException;
import java.awt.Frame;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

//import System.IO;
import java.io.File;
import java.nio.file.Files;

import java.nio.file.Path;
import java.nio.file.Paths;

import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;


import org.sikuli.script.FindFailed;
import org.sikuli.script.Key;

public class MouseMove2{
	public static void main(String[] args) throws Exception {
		
		System.setProperty("webdriver.chrome.driver", "C:\\VP\\tools\\chromedriver_win32\\chromedriver.exe");
//		System.setProperty("webdriver.gecko.driver", "C:\\VP\\tools\\geckodriver-v0.24.0-win64\\geckodriver.exe");
		WebDriver driver=new ChromeDriver();
//		WebDriver driver=new FirefoxDriver();
		driver.manage().window().maximize();
		driver.manage().deleteAllCookies();
		driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		
		driver.get("https://www.spicejet.com");
//		.click();
//		Going to Spicejet site and traversing through Login/Signup->Spiceclub members->Member login->Sign up 
//		and finally clicking on Sign up option 
		Actions act=new Actions(driver);
		act.moveToElement(driver.findElement(By.xpath("//a[@id='ctl00_HyperLinkLogin']")))
		.moveToElement(driver.findElement(By.xpath("//a[contains(text(), 'SpiceClub Members')]")))
		.click()
		.moveToElement(driver.findElement(By.xpath("//a[@href='javascript:void();' and contains(text(), 'Continue')]/preceding::a[@href='javascript:void();']/following::ul[1]/li[1]")))
		.moveToElement(driver.findElement(By.xpath("//a[@href='https://book.spicejet.com/Register.aspx']")))
		.click()
		.build().perform();
		
		WebDriverWait wait=new WebDriverWait(driver,30);
		wait.until(ExpectedConditions.elementToBeClickable(By.id("personalInputContent")));
		assertTrue(driver.findElement(By.id("personalInputContent")).isDisplayed());
//		driver.quit();
		System.out.println("Driver quit");
		
	}
}