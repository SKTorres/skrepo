//Not working


package com.qa.tests;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import java.awt.AWTException;
import java.awt.Frame;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

//import System.IO;
import java.io.File;
import java.nio.file.Files;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;


import org.sikuli.script.FindFailed;
import org.sikuli.script.Key;

public class HtmlUnitDriverConcept{
	public static void main(String[] args) throws Exception {
		
//		System.setProperty("webdriver.chrome.driver", "C:\\VP\\tools\\chromedriver_win32\\chromedriver.exe");
//		WebDriver driver=new ChromeDriver();
		
//		1. HtmlUnitDriver is not available in Selenium 3.x version
//		2. To use this headless browser concept, we need to download htmlunitdriver jar file.
		
//		Advantages: 
//		1. testing is happening behind the scene - no browser is required. 
//		2. Very fast execution of the test cases. 
//		Disadvantages
//		1. Used for simple test cases. Fails when DOM is very complicated. 
//		2. Not suitable for Actions class operations --- User operations such as mouse movement, double click, 
//		drap and drop etc. 
		
//		Sometimes it is called Ghost browser,Headless browser. Other examples are PhantomJS --- Javascript. 
		
		WebDriver driver=new HtmlUnitDriver();
//		System.setProperty("webdriver.gecko.driver", "C:\\VP\\tools\\geckodriver-v0.24.0-win64\\geckodriver.exe");
//		WebDriver driver=new FirefoxDriver();
		
		driver.manage().window().maximize();
		driver.manage().deleteAllCookies();
		driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.get("https://ui.freecrm.com");
		
		System.out.println("The driver title before login is --> "+driver.getTitle());
		
		WebDriverWait wait=new WebDriverWait(driver,30);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@placeholder='E-mail address']")));
		
		driver.findElement(By.xpath("//input[@placeholder='E-mail address']")).sendKeys("snehashishkumar92@gmail.com");
		driver.findElement(By.xpath("//input[@name='password']")).sendKeys("Test123"); 
		driver.findElement(By.xpath("//div[contains(text(), 'Login') and @class='ui fluid large blue submit button']")).click();
		
		wait=new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[@class='user-display']")));
		
		System.out.println("The driver title after login is --> "+driver.getTitle());
//		driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
//		Thread.sleep(2000);
		driver.quit();
		System.out.println("Driver quit");
	}
}
