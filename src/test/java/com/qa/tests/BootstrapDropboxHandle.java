package com.qa.tests;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import java.awt.AWTException;
import java.awt.Frame;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

//import System.IO;
import java.io.File;
import java.nio.file.Files;

import java.nio.file.Path;
import java.nio.file.Paths;

import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;


import org.sikuli.script.FindFailed;
import org.sikuli.script.Key;

public class BootstrapDropboxHandle{
	public static void main(String[] args) throws Exception {
		
		System.setProperty("webdriver.chrome.driver", "C:\\VP\\tools\\chromedriver_win32\\chromedriver.exe");
		WebDriver driver=new ChromeDriver();
		
		driver.manage().window().maximize();
		driver.manage().deleteAllCookies();
		driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.get("https://www.jquery-az.com/boots/demo.php?ex=63.0_3");
		
		driver.findElement(By.xpath("//span[@class='multiselect-selected-text']")).click();
		
		WebDriverWait wait=new WebDriverWait(driver,30);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//ul[@class='multiselect-container dropdown-menu']/li[@class='active']/a/label")));
		
		List<WebElement> list=driver.findElements(By.xpath("//ul[@class='multiselect-container dropdown-menu']/li[@class='active']/a/label"));
		assertEquals(list.size(),2);
		assertEquals(list.get(0).getText(),"HTML");
		assertEquals(list.get(1).getText(),"CSS");	
		
		list=driver.findElements(By.xpath("//li[@class='multiselect-item multiselect-group']/a/label/b"));		
		System.out.println("The headers are :-");
		for(int i=0;i<list.size();i++) {
			System.out.println(list.get(i).getText());
		}
		list=driver.findElements(By.xpath("//a[@tabindex='0']"));		
		System.out.println("The remaining options are :-");
		for(int i=0;i<list.size();i++) {
			System.out.println(list.get(i).getText());
		}
		driver.findElement(By.xpath("//li[@class='multiselect-item multiselect-group']/a/label/b[contains(text(),' Web Development')]")).click();
		list=driver.findElements(By.xpath("//a[@tabindex='0']/label/input"));
		for(int i=0;i<5;i++) {
			assertTrue(list.get(i).isSelected());
		}
		
		Thread.sleep(2000);
		driver.quit();
		System.out.println("Driver quit.");
		
	}
}