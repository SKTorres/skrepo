//Not complete since not working. 

package com.qa.tests;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import java.awt.AWTException;
import java.awt.Frame;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

//import System.IO;
import java.io.File;
import java.nio.file.Files;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;


import org.sikuli.script.FindFailed;
import org.sikuli.script.Key;

public class CalendarSelectTest{
	public static void main(String[] args) throws Exception {
		
		System.setProperty("webdriver.chrome.driver", "C:\\VP\\tools\\chromedriver_win32\\chromedriver.exe");
		WebDriver driver=new ChromeDriver();
		
		driver.manage().window().maximize();
		driver.manage().deleteAllCookies();
		driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.get("https://ui.freecrm.com");
		
		WebDriverWait wait=new WebDriverWait(driver,30);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@placeholder='E-mail address']")));
		driver.findElement(By.xpath("//input[@placeholder='E-mail address']")).sendKeys("snehashishkumar92@gmail.com");
		driver.findElement(By.xpath("//input[@name='password']")).sendKeys("Test123");
		driver.findElement(By.xpath("//div[contains(text(), 'Login') and @class='ui fluid large blue submit button']")).click();
		
		wait=new WebDriverWait(driver,30);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[contains(text(), 'Calendar')]")));
		driver.findElement(By.xpath("//span[contains(text(), 'Calendar')]")).click();
		String date="07-November-1994";
		String dateArr[]=date.split("-");
		int d=Integer.parseInt(String.valueOf(dateArr[0]));
		String day=Integer.toString(d);
		String month=dateArr[1];
		String year=dateArr[2];
		
		String monthYearVal=driver.findElement(By.xpath("//span[@class='rbc-toolbar-label']")).getText();
		System.out.println(monthYearVal);
		
		Date todayDate=new Date();
		DateFormat df=new SimpleDateFormat("dd/MM/yyyy");
		String td=df.format(todayDate);
		System.out.println("Today's date is --->"+td);
		
		wait=new WebDriverWait(driver,30);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='rbc-day-bg rbc-today']")));
//		//div[@class='rbc-day-bg rbc-today'] --> Xpath of today's date.
//		//div[@class='rbc-date-cell']-->Xpath of all dates excluding today's date and the vacant dates of the month.
		String dateVal=driver.findElement(By.xpath("//div[@class='rbc-day-bg rbc-today']")).getText();
		System.out.println(dateVal);
		
		
		
		driver.quit();
		System.out.println("Driver quit.");
	}
}