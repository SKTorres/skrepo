package com.qa.tests;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import java.awt.AWTException;
import java.awt.Frame;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

//import System.IO;
import java.io.File;
import java.nio.file.Files;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;


import org.sikuli.script.FindFailed;
import org.sikuli.script.Key;

public class CalendarUsingJS{
	public static void main(String[] args) throws Exception {
		
		System.setProperty("webdriver.chrome.driver", "C:\\VP\\tools\\chromedriver_win32\\chromedriver.exe");
		WebDriver driver=new ChromeDriver();
		
		driver.manage().window().maximize();
		driver.manage().deleteAllCookies();
		driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.get("https://www.spicejet.com/");
		WebDriverWait wait=new WebDriverWait(driver,30);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@id='ctl00_mainContent_ddl_originStation1_CTXT']")));
		
		driver.findElement(By.xpath("//span[@id='ctl00_mainContent_ddl_originStation1_CTXTaction']")).click();
//		//input[@id='ctl00_mainContent_ddl_originStation1_CTXT']
		driver.findElement(By.xpath("//a[@text='Bengaluru (BLR)']")).click();
		
		driver.findElement(By.xpath("//input[@id='ctl00_mainContent_ddl_destinationStation1_CTXT']")).click();
		driver.findElement(By.xpath("//a[@text='Bagdogra (IXB)']//following::a[@text='Bagdogra (IXB)']")).click();
		
//		WebElement ele= driver.findElement(By.xpath("//input[@id='ctl00_mainContent_txt_Fromdate']"));
		
//		Thread.sleep(5000);
//		selectDateByJS(driver, ele, "23/03/2020");
		
		String fromDate="21-January-2021";
		String toDate="27-February-2021";
		String dateArr[]=splitDate(fromDate);
		String firstMonthValidDates="//div[@class='ui-datepicker-group ui-datepicker-group-first']//child::table[@class='ui-datepicker-calendar']/tbody/tr/td/a";
		String firstMonthXpath="//span[@class='ui-datepicker-month']//preceding::span[@class='ui-datepicker-month']";
		String firstYearXpath="//span[@class='ui-datepicker-year']//preceding::span[@class='ui-datepicker-year']";

		String secondMonthValidDates="//div[@class='ui-datepicker-group ui-datepicker-group-last']//child::table[@class='ui-datepicker-calendar']/tbody/tr/td/a";
		String secondMonthXpath="//span[@class='ui-datepicker-month']//following::span[@class='ui-datepicker-month']";
		String secondYearXpath="//span[@class='ui-datepicker-year']//following::span[@class='ui-datepicker-year']";
		String xpathDepartureDateBox="//input[@id='ctl00_mainContent_view_date1']";
		String xpathReturnDateBox="//input[@id='ctl00_mainContent_view_date2']";
				
		System.out.println(dateArr[0]+" "+dateArr[1]+" "+dateArr[2]);
		if(!(chooseDate(driver, firstMonthXpath, firstYearXpath, dateArr, firstMonthValidDates, xpathDepartureDateBox)))
			if(!(chooseFromSingleMonth(driver, firstMonthXpath, firstYearXpath, dateArr, firstMonthValidDates)))
				if(!(chooseFromSingleMonth(driver, secondMonthXpath, secondYearXpath, dateArr, secondMonthValidDates)))
					System.out.println("Provide valid date.");
		dateArr=splitDate(toDate);
		System.out.println(dateArr[0]+" "+dateArr[1]+" "+dateArr[2]);
		if(!(chooseDate(driver, firstMonthXpath, firstYearXpath, dateArr, firstMonthValidDates, xpathReturnDateBox)))
			if(!(chooseFromSingleMonth(driver, firstMonthXpath, firstYearXpath, dateArr, firstMonthValidDates)))
				if(!(chooseFromSingleMonth(driver, secondMonthXpath, secondYearXpath, dateArr, secondMonthValidDates)))
					System.out.println("Provide valid date.");
		
		driver.findElement(By.id("ctl00_mainContent_btn_FindFlights")).click();
		String idFinalAmt="spanamnt";
		wait=new WebDriverWait(driver, 45);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(idFinalAmt)));
		System.out.println("The cheapest price is - Rs. "+driver.findElement(By.id(idFinalAmt)).getText());
		
		//td[@class='fareCol1 col-active SelectedFareTD']//child::span[@class='flightfare']
//		driver.quit();
		
		System.out.println("Driver quit.");
	}
	
	public static boolean chooseDate(WebDriver driver, String monthXpath, String yearXpath, String[] dateArr, String ValidDates, String dateBox) {
		
		int f=0;
		String xpathNextBtn="//a[contains(@class, 'ui-datepicker-next ui-corner-all')]";
		driver.findElement(By.xpath(dateBox)).click();
		while(isClickable(driver, xpathNextBtn)){
			String month=driver.findElement(By.xpath(monthXpath)).getText();		
			String year=driver.findElement(By.xpath(yearXpath)).getText();
//			System.out.println(month +" "+year);
			
			if(month.equalsIgnoreCase(dateArr[1]) && year.equalsIgnoreCase(dateArr[2])) {
				List<WebElement> list=driver.findElements(By.xpath(ValidDates));
				for(int i=0;i<list.size();i++) {
					if(dateArr[0].equals(list.get(i).getText())) {
						list.get(i).click();
						return true;
					}
				}
			}
			else
				driver.findElement(By.xpath("//a[contains(@class, 'ui-datepicker-next ui-corner-all')]")).click();
		}
		return false;
	}
	public static boolean chooseFromSingleMonth(WebDriver driver, String monthXpath, String yearXpath, String[] dateArr, String validDates) {
		String month=driver.findElement(By.xpath(monthXpath)).getText();		
		String year=driver.findElement(By.xpath(yearXpath)).getText();
//		System.out.println(month +" "+year);
		
		if(month.equalsIgnoreCase(dateArr[1]) && year.equalsIgnoreCase(dateArr[2])) {
			List<WebElement> list=driver.findElements(By.xpath(validDates));
			for(int i=0;i<list.size();i++) {
				if(dateArr[0].equals(list.get(i).getText())) {
					list.get(i).click();
					return true;
				}
			}
		}
		return false;
	}
	
	
	public static String[] splitDate(String str) {
		String dateArr[]=str.split("-");
		return dateArr;
	}
	
	public static boolean isClickable(WebDriver driver, String xpath) {
		
		String val=driver.findElement(By.xpath(xpath)).getCssValue("opacity");
//		System.out.println(val);
		if(val.equals("1"))
			return true;
		else
			return false;
	}
	
	public static void selectDateByJS(WebDriver driver, WebElement ele, String dateVal) {
		JavascriptExecutor js=((JavascriptExecutor) driver);
		js.executeScript("arguments[0].setAttribute('value','"+dateVal+"')", ele);
	}
}