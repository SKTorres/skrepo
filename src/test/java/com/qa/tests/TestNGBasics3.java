package com.qa.tests;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

//import System.IO;
import java.io.File;
import java.io.FileInputStream;
import java.nio.file.Files;

import java.nio.file.Path;
import java.nio.file.Paths;

import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Key;

public class TestNGBasics3{
	
	WebDriver driver;
	
	@Test(invocationCount=4)
	public void sum() {
		int a=10;
		int b=20;
		int c=a+b;
		System.out.println("Sum is "+c);
	}
		
	@Test(invocationTimeOut=2000)
	public void infiniteLoopTest() {
		int i=1;
		while(i>0) {
			System.out.println("i= "+i);
		}
	}
	
	@Test(expectedExceptions=NumberFormatException.class)
	public void test1() {
		String a="100A";
		int i=Integer.parseInt(a);
		System.out.println(i);
	}
	
}

		