package com.qa.tests;

import static org.testng.Assert.assertTrue;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

//import System.IO;
import java.io.File;
import java.nio.file.Files;

import java.nio.file.Path;
import java.nio.file.Paths;

import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;


import org.sikuli.script.FindFailed;
import org.sikuli.script.Key;

public class WordToPDF {
	public static void main(String[] args) throws Exception {
//		System.setProperty("webdriver.chrome.driver","C:\\VP\\tools\\chromedriver_win32\\chromedriver.exe");
//		WebDriver driver= new ChromeDriver();
		System.setProperty("webdriver.gecko.driver", "C:\\VP\\tools\\geckodriver-v0.24.0-win64\\geckodriver.exe");
		WebDriver driver = new FirefoxDriver();
		
//		System.setProperty("webdriver.edge.driver", "C:\\VP\\tools\\microsoftedgedriver\\MicrosoftWebDriver.exe");
//		WebDriver driver= new EdgeDriver();
		
		driver.manage().deleteAllCookies();
		driver.manage().window().maximize();
		String url = "https://www.ilovepdf.com/";
		driver.get(url);
		String XPATH_WORD_TO_PDF = "//h3[contains(text(), 'Word to PDF')]";
		driver.findElement(By.xpath(XPATH_WORD_TO_PDF)).click();
		String XPATH_UPLOAD_BTN = "//a[@id='pickfiles']";
		String XPATH_PROCEED_BTN = "//button[@id='processTask']";
		String doc = "C:\\Users\\KumarSne\\Documents\\Resume_SnehashishKumar.docx";
		String FileName = "Resume_SnehashishKumar";
		String currWin = driver.getWindowHandle();
		System.out.println(currWin);

		String DownloadPath = "C:\\Users\\KumarSne\\Downloads";
		driver.findElement(By.xpath(XPATH_UPLOAD_BTN)).click();

		Screen screen = new Screen();
		Pattern pattern = new Pattern("C:\\Users\\KumarSne\\Desktop\\Auto\\Upload.PNG");
//		screen.click(pattern); 
		screen.wait(pattern, 30);
		
//		String imagePath = System.getProperty("user.dir") + "\\src\\main\\resources\\Images\\"+fileName;
//		System.out.println(doc);
		screen.type(pattern, doc);
		screen.type(Key.ENTER);
		driver.switchTo().window(currWin);
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(XPATH_PROCEED_BTN)));

		System.out.println(((RemoteWebDriver) driver).getCapabilities().getBrowserName());
		driver.findElement(By.xpath(XPATH_PROCEED_BTN)).click();

		/*
		 * if(Directory.Exists(DownloadPath)){ boolean checkFile=CheckFile(FileName); if
		 * (checkFile){ } else{ throw new java.lang.Exception(); } }
		 */

		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		Thread.sleep(30000);
		
		Pattern pat = new Pattern("C:\\Users\\KumarSne\\Desktop\\Auto\\Ok.PNG"); 
		screen.wait(pat, 10);
		screen.click();
//		screen.type(pat, Key.ENTER);

		
		
//		Robot robot= new Robot();
//		robot.keyPress(KeyEvent.VK_ENTER);
		
//		if(((RemoteWebDriver)
//			driver).getCapabilities().getBrowserName().equals("firefox")) { Actions
//			act=new Actions(driver); act.sendKeys(Keys.ENTER).build().perform(); Robot
//			robot = new Robot(); robot.keyPress(KeyEvent.VK_ENTER); //
//			robot.keyRelease(KeyEvent.VK_ENTER);
//		  
//		}
//		  

		driver.switchTo().window(currWin);

		System.out.println(DownloadPath + "\\" + FileName + ".pdf");
		File file = new File(DownloadPath + "\\" + FileName + ".pdf");
		System.out.println(file.exists());

//		if(!((RemoteWebDriver) driver).getCapabilities().getBrowserName().equals("internet explorer"))
//			driver.switchTo().window(currWin);

		if (!file.exists()) {
			System.out.println("File does not exist.");
			// throw new java.lang.Exception();
		}
		assertTrue(file.exists());
		/*
		 * System.out.println(DownloadPath+FileName);
		 * 
		 * Path path=Paths.get(DownloadPath+FileName);
		 * System.out.println(Files.exists(path)); if(Files.notExists(path)){ throw new
		 * java.lang.Exception(); } else{ System.out.println("File is present."); }
		 */

//		File dir = new File(DownloadPath);
//		boolean checkDir=dir.exists(DownloadPath+FileName);
//		if (checkDir){

//		}
//		else{
//			throw new java.lang.Exception();
//		}

//		WebElement uploadElement = driver.findElement(By.xpath(XPATH_UPLOAD_BTN));
//		uploadElement.sendKeys("C:\\Users\\Snehashish Kumar\\Documents\\PAKISTAN AND AFGHANISTAN");
//	    driver.switchTo()
//	            .activeElement()
//	            .sendKeys(
//	                    "C:\\Users\\Snehashish Kumar\\Documents\\PAKISTAN AND AFGHANISTAN");
//	    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

//		WebDriverWait wait = new WebDriverWait(driver, 30);
//		wait.until(ExpectedConditions.alertIsPresent());

		// switch to the file upload window
		// Alert alert = driver.switchTo().alert();

		// enter the filename
		// alert.sendKeys(fileName);

		// hit enter
		// Robot r = new Robot();
		// r.keyPress(KeyEvent.VK_ENTER);
		// r.keyRelease(KeyEvent.VK_ENTER);

		// switch back
		// driver.switchTo().activeElement();

//		driver.findElement(By.xpath(XPATH_PROCEED_BTN)).click();
		driver.switchTo().window(currWin);
		driver.quit();
		System.out.println("Driver quit");
	}
}
